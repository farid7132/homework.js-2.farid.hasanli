

let num1 = prompt("Enter first number");
while (isNaN(num1) || num1 === "" || num1 === undefined|| num1 === null){
    num1 = prompt("Enter a correct number")
}

let num2 = prompt("Enter second number");
while (isNaN(num2) || num2 === "" || num2 === undefined || num2 === null){
    num2 = prompt("Enter a correct number")
}

num1 = parseFloat(num1);
num2 = parseFloat(num2);

let opr = prompt("Enter one of the operations : + ;  - ; * ; / ");

function calc(firstNum,secondNum,myOpr) {
    let result;
    if (myOpr === "+"){
        result = firstNum + secondNum;
        return result;
    }else if (myOpr === "-"){
        result = firstNum - secondNum;
        return result;
    }else if (myOpr === "*"){
        result =firstNum * secondNum;
        return result;
    }
    else if (myOpr === "/"){
        result = firstNum / secondNum;
        return result;
    }
}

console.log(calc(num1, num2, opr));
