## 1
Functions are sections of code that are used to perform a particular task. They can be used to avoid repetition of commands within the program. 
If you have operations that are performed in various different parts of the program, then it makes sense to remove the repeated code 
and create a separate function or procedure that can be called from those places instead. Not only will this reduce the size of your program, 
but it will make the code easier to maintain, and it will remove the possibility that some of the code segments are updated, but not others.
Function takes an input , does some calculations on the input, and then gives back a result.
A function in a programming language is a program fragment that knows how to perform a defined task. 

## 2
Arguments defined in a function because we need to know what input the function expects from us in order to do some operation with these inputs
and give us the expected result.